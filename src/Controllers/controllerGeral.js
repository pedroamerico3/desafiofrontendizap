import Api from "../Config/Api";

export default async function controllerGeral(amount, installments) {
  try {
    const response = await Api.post("", {
      amount,
      installments,
    });
    const allData = [response.data];
    return allData;
  } catch (err) {
    console.log(err);
  }
}
