import axios from "axios";

const api = axios.create({
  baseURL: "https://izap-frontend-challenges.herokuapp.com",
  header: ["content-type: application/json"],
});

export default api;
