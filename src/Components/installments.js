import React from "react";
import "./installments.css";

export default function Installments(props) {
  const { installments, setInstallments, getResults } = props;
  return (
    <div>
      <p className="firstTextAmount">Em quantas parcelas*</p>
      <div className="firsContentAmount">
        <h1>{installments}&nbsp;parcelas</h1>
        <div className="groupButtons">
          <button
            className="buttonProp material-icons"
            onClick={() => setInstallments(installments - 1) + getResults()}
          >
            remove
          </button>
          <button
            className="buttonProp material-icons"
            onClick={() => setInstallments(installments + 1) + getResults()}
          >
            add
          </button>
        </div>
      </div>
      <p className="secondTextAmount">
        Lorem ipsum dolor sit amert, consectetur adipscing slit.
      </p>
      <p className="secondTextAmount">
        Morbi eu magna bibendum, fringilla orci ac, ornare dolor.
      </p>
    </div>
  );
}
