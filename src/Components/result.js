import React, { useEffect } from "react";
import "./result.css";

export default function Result(props) {
  const { results } = props;

  return (
    <div className="card">
      <div className="firstResult">
        <p> Amanhã voce receberá</p>
        <p className="resultCalc">
          R$&nbsp;{parseFloat(results[0][1]).toFixed(2)}
        </p>
      </div>
      <div className="firstResult">
        <p> em 15 dias voce receberá</p>
        <p className="resultCalc">
          R$&nbsp;{parseFloat(results[0][15]).toFixed(2)}
        </p>
      </div>
      <div className="firstResult">
        <p> em 30 dias voce receberá</p>
        <p className="resultCalc">
          R$&nbsp;{parseFloat(results[0][30]).toFixed(2)}
        </p>
      </div>
    </div>
  );
}
