import React from "react";
import "./amount.css";

export default function Amount(props) {
  const { amount, setAmount, getResults } = props;

  return (
    <div>
      <p className="firstTextAmount">Valor da venda*</p>
      <div className="firsContentAmount">
        <h1>R$&nbsp;{parseFloat(amount).toFixed(2)}</h1>
        <div className="groupButtons">
          <button
            className="buttonProp material-icons"
            onClick={() => setAmount(amount - 1) + getResults()}
          >
            remove
          </button>
          <button
            className="buttonProp material-icons"
            onClick={() => setAmount(amount + 1) + getResults()}
          >
            add
          </button>
        </div>
      </div>
    </div>
  );
}
