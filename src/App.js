import React, { useState, useEffect } from "react";
import ControllerGeral from "./Controllers/controllerGeral";
import Amount from "./Components/amount";
import Installments from "./Components/installments";
import Result from "./Components/result";
import ImgIzap from "./Assets/imgIzap.png";

import "./App.css";

function App() {
  const [amount, setAmount] = useState(1000);
  const [installments, setInstallments] = useState(3);
  const [results, setResults] = useState([{ 1: 0, 15: 0, 30: 0, 90: 0 }]);

  async function getResults() {
    const dataResponse = await ControllerGeral(amount, installments);
    if (dataResponse) {
      setResults(dataResponse);
    }
  }

  useEffect(() => {
    getResults();
    // eslint-disable-next-line
  }, []);

  return (
    <div className="App">
      <div className="header">
        <img src="https://izap.com.br/images/Guia-de-identidade-visual---Izap-Softworks_%C3%81rea-de-prote%C3%A7%C3%A3o.png" />
        <span className="material-icons">menu</span>
      </div>
      <div className="firstContent">
        <p className="firstText">Simule sua antecipação</p>
        <div className="line"></div>
        <p className="secondText">
          Antecipe as suas parcelas com muito mais segurança
        </p>
      </div>
      <div className="secondContent">
        <img src={ImgIzap} />
      </div>
      <div className="headerThirdContent">
        <p>Faça sua simulação</p>
      </div>
      <div className="firstInput">
        <Amount amount={amount} setAmount={setAmount} getResults={getResults} />
        <Installments
          installments={installments}
          setInstallments={setInstallments}
          getResults={getResults}
        />
      </div>
      <div className="result">
        <Result results={results} setResults={setResults} />
      </div>
    </div>
  );
}

export default App;
